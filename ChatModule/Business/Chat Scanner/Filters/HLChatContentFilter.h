//
//  HLChatContentFilter.h
//  ChatModule
//
//  Created by Huy Le on 3/25/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACSignal;
@interface HLChatContentFilter : NSObject

/**
 *  Filtering Text Method
 *  Use to test filter's algorithms
 *
 *  @return Array of filtered text
 */
- (NSArray *)filterRawContent:(NSString *)rawContent;

/**
 *  Signal for filtering text
 */
- (RACSignal *)filterSignalWithContent:(NSString *)rawContent;

@end
