//
//  HLChatContentFilter.m
//  ChatModule
//
//  Created by Huy Le on 3/25/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import "HLChatContentFilter.h"

@implementation HLChatContentFilter

- (NSArray *)filterRawContent:(NSString *)rawContent
{
    NSAssert(NO, @"Subclass have to override this method!");
    return nil;
}

- (RACSignal *)filterSignalWithContent:(NSString *)rawContent
{
    NSAssert(NO, @"Subclass have to override this method to plug into Scanner system!");
    return nil;
}

@end
