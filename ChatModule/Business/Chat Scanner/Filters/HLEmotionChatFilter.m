//
//  HLEmotionChatFilter.m
//  ChatModule
//
//  Created by Huy Le on 3/25/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import "HLEmotionChatFilter.h"
#import "HLEmotionModel.h"
#import <ReactiveCocoa.h>

@implementation HLEmotionChatFilter

- (NSArray *)filterRawContent:(NSString *)rawContent
{
    NSScanner *scanner = [[NSScanner alloc] initWithString:rawContent];
    NSMutableArray *emotionObjects = [NSMutableArray new];
    
    while (!scanner.isAtEnd) {
        
        NSString *anExpectedEmotionShortcut = [NSString new];
        
        [self use:scanner scanTo:@"("];
        [scanner scanCharactersFromSet:[self emotionSupportedCharacterSet] intoString:&anExpectedEmotionShortcut];
        
        if (scanner.isAtEnd) { break; }
      
        if ([self nextCharacterIn:scanner is:@")"] &&
            (anExpectedEmotionShortcut.length > 0) &&
            (anExpectedEmotionShortcut.length <= 15)) {
            
            NSString *emotionShortcut = anExpectedEmotionShortcut;
            HLEmotionModel *emotion = [[HLEmotionModel alloc] initWithInfos:@{HLEmotionShortcutKeyInfos:emotionShortcut}];
            [emotionObjects addObject:emotion];
        }
    }
    
    return emotionObjects;
}

- (void)use:(NSScanner *)scanner scanTo:(NSString *)stopString
{
    [scanner scanUpToString:stopString intoString:NULL];
    [scanner scanString:stopString intoString:NULL];
}

- (NSCharacterSet *)emotionSupportedCharacterSet
{
    static NSCharacterSet *supportedCharacterSet;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        supportedCharacterSet = [NSCharacterSet lowercaseLetterCharacterSet];
    });
    return supportedCharacterSet;
}

- (BOOL)nextCharacterIn:(NSScanner *)scanner is:(NSString *)expetedString
{
    NSString *nextChar = [scanner.string substringWithRange:NSMakeRange(scanner.scanLocation, 1)];
    return [nextChar isEqualToString:expetedString];
}

- (RACSignal *)filterSignalWithContent:(NSString *)rawContent
{
    RACSignal *filterSignal =
    [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        NSArray *emotionObjects = [self filterRawContent:rawContent];
        [subscriber sendNext:emotionObjects];
        [subscriber sendCompleted];
        return nil;
    }];
    return filterSignal;
}

@end
