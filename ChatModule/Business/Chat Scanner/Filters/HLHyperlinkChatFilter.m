//
//  HLHyperlinkChatFilter.m
//  ChatModule
//
//  Created by Huy Le on 3/25/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import "HLHyperlinkChatFilter.h"
#import "HLHyperlinkModel.h"
#import "HLWebParser.h"
#import <ReactiveCocoa.h>

@implementation HLHyperlinkChatFilter

- (NSArray *)filterRawContent:(NSString *)rawContent
{
    NSRegularExpression *regex = [self regexPattern];
    NSArray *matches = [regex matchesInString:rawContent
                                      options:NSMatchingReportProgress
                                        range:NSMakeRange(0, rawContent.length)];
    
    NSArray *urls = [[[matches rac_sequence]
                                map:^id(NSTextCheckingResult *textCheckingResult) {
        
                                    NSRange urlRange = textCheckingResult.range;
                                    NSString *url = [rawContent substringWithRange:urlRange];
                                    return url;
                                }] array];
   
    return urls;
}

- (NSString *)diagoPeriniRegexPattern
{
    static NSString *diagoRegexPattern;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        diagoRegexPattern = @"(?:(?:https?|ftp):\\/\\/)?(?:\\S+(?::\\S*)?@)?(?:(?!(?:10|127)(?:\\.\\d{1,3}){3})(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))(?::\\d{2,5})?(?:\\/\\S*)?";
    });
    return diagoRegexPattern;
}

- (NSRegularExpression *)regexPattern
{
    NSError *error;
    NSRegularExpression *regexPattern = [NSRegularExpression regularExpressionWithPattern:[self diagoPeriniRegexPattern]
                                                                                  options:NSRegularExpressionAnchorsMatchLines
                                                                                    error:&error];
    NSAssert(error == nil, @"Found error: %@", error);
    return regexPattern;
}

- (RACSignal *)filterSignalWithContent:(NSString *)rawContent
{
    RACSignal *signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        [[RACScheduler schedulerWithPriority:RACSchedulerPriorityBackground] schedule:^{
            
            NSArray *urls = [self filterRawContent:rawContent];
            RACSignal *hyperlinkObjectSignal = [self hyperlinkObjectSignalFromURL:urls];
            
            [hyperlinkObjectSignal
            subscribeNext:^(NSArray *hyperLinkObjects) {
                [subscriber sendNext:hyperLinkObjects];
            }];
        }];
        return nil;
    }];
    return signal;
    
}

- (RACSignal *)hyperlinkObjectSignalFromURL:(NSArray *)urls
{
    return  [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
                RACSignal *bundlePullTitleSignals =  [[urls rac_sequence] signal];
        
                NSMutableArray *bundle = [NSMutableArray new];
                [bundlePullTitleSignals subscribeNext:^(NSString *aUrl) {
                    
                    [[HLWebParser new] titleOfURL:aUrl
                                          success:^(NSString *title, NSString *url) {
                                              
                                              [bundle addObject:[self hyperLinkModelFromURL:url andTitle:title]];
                                              [subscriber sendNext:bundle];
                                          } failure:^(NSError *error, NSString *url) {
                                          }];
                } completed:^{
                }];
                return nil;
            }];
}

- (HLHyperlinkModel *)hyperLinkModelFromURL:(NSString *)url andTitle:(NSString *)title
{
    HLHyperlinkModel *hyperlink = [[HLHyperlinkModel alloc] initWithInfos:@{HLHyperLinkURLKeyInfos:url,
                                                                            HLHyperLinkTitleKeyInfos:title ? title : @"Not found"}];
    return hyperlink;
}

@end
