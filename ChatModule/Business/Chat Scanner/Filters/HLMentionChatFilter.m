//
//  HLMentionChatFilter.m
//  ChatModule
//
//  Created by Huy Le on 3/25/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import "HLMentionChatFilter.h"
#import "HLMentionModel.h"
#import <ReactiveCocoa.h>

@implementation HLMentionChatFilter

- (NSArray *)filterRawContent:(NSString *)rawContent
{
    NSScanner *scanner = [[NSScanner alloc] initWithString:rawContent];
    NSMutableArray *mentionObjects = [NSMutableArray new];
    
    while (!scanner.isAtEnd) {
        
        NSString *mentionName = @"";
        
        [self use:scanner scanTo:@"@"];
        [scanner scanUpToCharactersFromSet:[self nonWordCharacterSet] intoString:&mentionName];
        
        if (mentionName.length > 0) {
            HLMentionModel *mention = [[HLMentionModel alloc] initWithMention:mentionName];
            [mentionObjects addObject:mention];
        }
    }
    
    return mentionObjects;
}

- (NSCharacterSet *)nonWordCharacterSet
{
    static NSCharacterSet *nonWordCharacterSet;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSMutableCharacterSet *wordCharacterSet = [NSMutableCharacterSet lowercaseLetterCharacterSet];
        [wordCharacterSet formUnionWithCharacterSet:[NSCharacterSet uppercaseLetterCharacterSet]];
        [wordCharacterSet formUnionWithCharacterSet:[NSCharacterSet decimalDigitCharacterSet]];
        [wordCharacterSet addCharactersInString:@"_"];
        nonWordCharacterSet = [wordCharacterSet invertedSet];
    });
    return nonWordCharacterSet;
}

- (void)use:(NSScanner *)scanner scanTo:(NSString *)stopString
{
    [scanner scanUpToString:stopString intoString:NULL];
    [scanner scanString:stopString intoString:NULL];
}

- (RACSignal *)filterSignalWithContent:(NSString *)rawContent
{
    RACSignal *filterSignal =
    [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        NSArray *mentionObjects = [self filterRawContent:rawContent];
        [subscriber sendNext:mentionObjects];
        [subscriber sendCompleted];
        return nil;
    }];
    return filterSignal;
}

@end
