//
//  HLChatScanner.h
//  ChatModule
//
//  Created by Huy Le on 3/25/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACSignal;

@interface HLChatScanner : NSObject

/**
 *  Init scanner with some filters which subclass from HLChatContentFilter
 *
 *  @param filters Array of Filters
 *
 *  @return self
 */
- (instancetype)initWithFilters:(NSArray *)filters;

/**
 *  Return a signal, contains a json data value of filtered text
 *
 *  @param rawContent The text you want to filter
 *
 *  @return A signal, contains JSONData (NSData)
 */
- (RACSignal *)signalToScan:(NSString *)rawContent;

@end
