//
//  HLChatScanner.m
//  ChatModule
//
//  Created by Huy Le on 3/25/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import "HLChatScanner.h"
#import "HLJSONTransformer.h"
#import "HLChatContentFilter.h"
#import <ReactiveCocoa.h>

@interface HLChatScanner ()

@property (strong,nonatomic) NSArray *filters;

@end

@implementation HLChatScanner

- (instancetype)initWithFilters:(NSArray *)filters
{
    self = [super init];
    if (self) {
        self.filters = filters;
    }
    return self;
}

- (RACSignal *)signalToScan:(NSString *)rawContent
{
    NSArray *filterSignals =    [[[self.filters rac_sequence]
                                map:^RACSignal *(HLChatContentFilter *theFilter) {
                                    return [[theFilter filterSignalWithContent:rawContent] startWith:@[]];
                                }]
                                array];
    
    return  [[[[RACSignal
            combineLatest:filterSignals]
            map:^id(RACTuple *allContentFilterObjects) {
        
                return allContentFilterObjects.allObjects;
            }]
            deliverOn:[RACScheduler schedulerWithPriority:RACSchedulerPriorityBackground]]
            map:^id(NSArray *contentObjects) {
        
                NSData *jsonData = [[HLJSONTransformer new] jsonDataFrom:contentObjects];
                return jsonData;
            }];
    
}

@end
