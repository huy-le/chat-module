//
//  HLJSONTransformer.h
//  ChatModule
//
//  Created by Huy Le on 3/25/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HLJSONTransformer : NSObject

/**
 *  Transform JSON Object (Array, Dictionary) to JSON binary data
 *
 *  @param contentObjects is an array of array.
 *  The subArray contains a ChatContentObject. Only have one type of content in one subArray.
 *  Ex: contentObjects = @[arrayOfMentions, arrayOfLinks]
 *  @return JSON binary data
 */
- (NSData *)jsonDataFrom:(NSArray *)contentObjects;

@end
