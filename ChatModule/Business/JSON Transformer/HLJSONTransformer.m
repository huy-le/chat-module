//
//  HLJSONTransformer.m
//  ChatModule
//
//  Created by Huy Le on 3/25/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import "HLJSONTransformer.h"
#import "HLChatContentObject.h"
#import "NSArray+FunctionalMethods.h"

@implementation HLJSONTransformer

- (NSData *)jsonDataFrom:(NSArray *)contentObjects
{
    if (contentObjects.count == 0) { return nil; }
    NSMutableDictionary *jsonObjects = [NSMutableDictionary new];
    for (NSArray *objects in contentObjects) {
        
        if (objects.count == 0) { continue; }
        NSString *key = [(HLChatContentObject *)objects.firstObject jsonKey];
        NSArray *value = [objects arrayByTransformingObjectsUsingBlock:^id(HLChatContentObject *chatContentObject) {
            return chatContentObject.jsonObject;
        }];
        NSAssert(key, @"Key is nil");
        NSAssert(value, @"Value is nil");
        NSDictionary *jsonObject = @{key:value};
        [jsonObjects addEntriesFromDictionary:jsonObject];
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonObjects options:NSJSONWritingPrettyPrinted error:&error];
    NSAssert(error == nil, @"Found error: %@", error);
    return jsonData;
}

@end
