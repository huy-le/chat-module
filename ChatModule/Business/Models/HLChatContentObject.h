//
//  HLChatContentObject.h
//  ChatModule
//
//  Created by Huy Le on 3/28/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HLChatContentObject : NSObject

@property (strong,nonatomic) NSDictionary *infos;

- (instancetype)initWithInfos:(NSDictionary *)infos;

#pragma mark - JSON parsing

/**
 *  Return a (NSArray or NSDictionary) contain object can be parse to data by NSJSONSerialization
 *
 */
- (id)jsonObject;
- (NSString *)jsonKey;

#pragma mark - Testing
/**
 *  Return any data you want to test on UnitTest
 *
 */
- (id)content;

@end
