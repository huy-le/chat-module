//
//  HLChatContentObject.m
//  ChatModule
//
//  Created by Huy Le on 3/28/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import "HLChatContentObject.h"

@implementation HLChatContentObject

- (instancetype)initWithInfos:(NSDictionary *)infos
{
    self = [super init];
    if (self) {
        _infos = infos;
    }
    return self;
}

#pragma mark - JSON format

- (id)jsonObject
{
    NSAssert(NO, @"Have to override this method to implement the JSON pattern for specific type!");
    return nil;
}

- (NSString *)jsonKey
{
    NSAssert(NO, @"Have to override this method to implement the JSON group key for specific type!");
    return nil;
}

#pragma mark - UnitTest

- (id)content
{
    #warning Should override this method to help implement UnitTest easier
    return nil;
}

@end
