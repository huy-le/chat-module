//
//  HLEmotionModel.h
//  ChatModule
//
//  Created by Huy Le on 3/28/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import "HLChatContentObject.h"

static const NSString *HLEmotionShortcutKeyInfos = @"shortcut";

@interface HLEmotionModel : HLChatContentObject

@end
