//
//  HLEmotionModel.m
//  ChatModule
//
//  Created by Huy Le on 3/28/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import "HLEmotionModel.h"

@implementation HLEmotionModel

- (id)jsonObject
{
    return self.infos[HLEmotionShortcutKeyInfos];
}

- (id)content
{
    return self.infos[HLEmotionShortcutKeyInfos];
}

- (NSString *)jsonKey
{
    return @"emotions";
}

@end
