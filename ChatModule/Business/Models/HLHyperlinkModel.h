//
//  HLHyperlinkModel.h
//  ChatModule
//
//  Created by Huy Le on 3/28/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import "HLChatContentObject.h"

static const NSString *HLHyperLinkURLKeyInfos = @"url";
static const NSString *HLHyperLinkTitleKeyInfos = @"title";

@interface HLHyperlinkModel : HLChatContentObject

@end
