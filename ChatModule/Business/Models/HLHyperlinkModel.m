//
//  HLHyperlinkModel.m
//  ChatModule
//
//  Created by Huy Le on 3/28/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import "HLHyperlinkModel.h"

@implementation HLHyperlinkModel

- (id)jsonObject
{
    return @{@"url":self.infos[HLHyperLinkURLKeyInfos],
             @"title":self.infos[HLHyperLinkTitleKeyInfos]};
}

- (id)content
{
    return self.infos[HLHyperLinkURLKeyInfos];
} 

- (NSString *)jsonKey
{
    return @"links";
}

@end
