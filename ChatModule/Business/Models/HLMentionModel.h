//
//  HLMentionModel.h
//  ChatModule
//
//  Created by Huy Le on 3/27/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import "HLChatContentObject.h"

static const NSString *HLMentionNameKeyInfos = @"name";

@interface HLMentionModel : HLChatContentObject

- (instancetype)initWithMention:(NSString *)mentionName;

@end
