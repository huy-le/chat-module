//
//  HLMentionModel.m
//  ChatModule
//
//  Created by Huy Le on 3/27/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import "HLMentionModel.h"

@implementation HLMentionModel

- (instancetype)initWithMention:(NSString *)mentionName
{
    self = [super initWithInfos:@{HLMentionNameKeyInfos:mentionName}];
    if (self) {
    }
    return self;
}

- (id)jsonObject
{
    return self.infos[HLMentionNameKeyInfos];
}

- (id)content
{
    return self.infos[HLMentionNameKeyInfos];
}

- (NSString *)jsonKey
{
    return @"mentions";
}

@end
