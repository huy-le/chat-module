//
//  HLNetworkProvider.h
//  ChatModule
//
//  Created by Huy Le on 3/29/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HLNetworkProvider : NSObject

- (void)getHTMLfromURL:(NSString *)url success:(void (^)(NSData *response))success failure:(void (^)(NSError *error))failure;

@end
