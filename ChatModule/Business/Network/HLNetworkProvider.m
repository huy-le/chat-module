//
//  HLNetworkProvider.m
//  ChatModule
//
//  Created by Huy Le on 3/29/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import "HLNetworkProvider.h"
#import <AFNetworking.h>
#import <UIKit+AFNetworking.h>

@implementation HLNetworkProvider

- (instancetype)init
{
    self = [super init];
    if (self) {
        [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    }
    return self;
}

- (void)setupAFNetworkingManager:(AFHTTPRequestOperationManager *)manager
{
    AFHTTPResponseSerializer *responseSerializer = [AFHTTPResponseSerializer serializer];
    responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html"]];
    manager.responseSerializer = responseSerializer;
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    [requestSerializer setValue:@"" forHTTPHeaderField:@"User-Agent"];
    manager.requestSerializer = requestSerializer;
}

- (void)getHTMLfromURL:(NSString *)url success:(void (^)(NSData *))success failure:(void (^)(NSError *))failure
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [self setupAFNetworkingManager:manager];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

@end
