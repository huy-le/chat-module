//
//  HLWebParser.h
//  ChatModule
//
//  Created by Huy Le on 3/29/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACSignal;
@interface HLWebParser : NSObject

- (void)titleOfURL:(NSString *)urlPathString
           success:(void (^)(NSString *title, NSString *url))success
           failure:(void (^)(NSError *error, NSString *url))failure;

@end
