//
//  HLWebParser.m
//  ChatModule
//
//  Created by Huy Le on 3/29/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import "HLWebParser.h"
#import "HLNetworkProvider.h"
#import <Ono.h>

@implementation HLWebParser

- (void)titleOfURL:(NSString *)urlPathString
           success:(void (^)(NSString *title, NSString *url))success
           failure:(void (^)(NSError *, NSString *url))failure
{
    [[HLNetworkProvider new] getHTMLfromURL:urlPathString success:^(NSData *response) {
        
        NSError *error;
        ONOXMLDocument *onoxmlDocument = [ONOXMLDocument HTMLDocumentWithData:response error:&error];
        ONOXMLElement *titleTagElement = [self firstElementsPassTest:^BOOL(ONOXMLElement *element) {
                                              BOOL isTitleTag = [element.tag isEqualToString:@"title"];
                                              return isTitleTag;
                                          } fromRootElement:onoxmlDocument.rootElement];
        NSString *title = titleTagElement.stringValue;
        if (success) {
            success(title, urlPathString);
        }
    } failure:^(NSError *error) {
        
        if (failure) {
            failure(error, urlPathString);
        }
    }];
}

- (ONOXMLElement *)firstElementsPassTest:(BOOL (^)(ONOXMLElement *element))block
                         fromRootElement:(ONOXMLElement *)rootElement
{
    __block ONOXMLElement *elementPassedTest;
    [self digIntoElements:rootElement block:^BOOL (ONOXMLElement *element) {
        
        BOOL test = block(element);
        if (test) {
            elementPassedTest = element;
            return YES;
        }
        return NO;
    }];
    return elementPassedTest;
}

- (void)digIntoElements:(ONOXMLElement *)element
                  block:(BOOL (^)(ONOXMLElement *element))block
{
    BOOL shouldStop = block(element);
    if (shouldStop) { return; }
    if (!element.children) { return; }
    for (ONOXMLElement *child in element.children) {
        [self digIntoElements:child block:block];
    }
}

@end
