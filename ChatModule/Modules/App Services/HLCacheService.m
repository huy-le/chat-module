//
//  HLCacheService.m
//  ChatModule
//
//  Created by Huy Le on 3/29/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import "HLCacheService.h"

@implementation HLCacheService

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:2 * 1024 * 1024
                                                            diskCapacity:50 * 1024 * 1024
                                                                diskPath:nil];
    [NSURLCache setSharedURLCache:sharedCache];
    return YES;
}

@end
