//
//  HLChatScreenViewModel.h
//  ChatModule
//
//  Created by Huy Le on 3/29/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACSignal;

@interface HLChatScreenViewModel : NSObject

@property (strong,nonatomic) NSString *jsonObjectPrettyFormat;

- (instancetype)initWithTextSignal:(RACSignal *)textSignal;

@end
