//
//  HLChatScreenViewModel.m
//  ChatModule
//
//  Created by Huy Le on 3/29/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import "HLChatScreenViewModel.h"
#import "HLChatScanner.h"
#import "HLMentionChatFilter.h"
#import "HLEmotionChatFilter.h"
#import "HLHyperlinkChatFilter.h"
#import <ReactiveCocoa.h>

@interface HLChatScreenViewModel ()

@property (strong,nonatomic) HLChatScanner *scanner;

@end

@implementation HLChatScreenViewModel

- (instancetype)initWithTextSignal:(RACSignal *)textSignal
{
    self = [super init];
    if (self) {
        NSArray *filters = @[[HLMentionChatFilter new],
                             [HLEmotionChatFilter new],
                             [HLHyperlinkChatFilter new]];
        self.scanner = [[HLChatScanner alloc] initWithFilters:filters];
        RAC(self, jsonObjectPrettyFormat) = [self bind:textSignal];
    }
    return self;
}

- (RACSignal *)bind:(RACSignal *)signal
{
    return
    [[[[[[signal throttle:1] distinctUntilChanged] map:^RACStream *(NSString *text) {
        return [self.scanner signalToScan:text];
    }] switchToLatest] map:^id(NSData *jsonData) {
        
        return [self getJsonPrettyPrintedFormatFrom:jsonData];
    }] deliverOn:[RACScheduler mainThreadScheduler]];
}

- (NSString *)getJsonPrettyPrintedFormatFrom:(NSData *)jsonData
{
    NSString *jsonPrettyFormat = [[[NSString alloc] initWithData:jsonData
                                                        encoding:NSUTF8StringEncoding]
                                  stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    return jsonPrettyFormat;
}

@end
