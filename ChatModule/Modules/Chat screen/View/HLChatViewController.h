//
//  HLChatViewController.h
//  ChatModule
//
//  Created by Huy Le on 3/25/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HLChatScreenViewModel;

@interface HLChatViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *chatTextField;
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;

@property (strong,nonatomic) HLChatScreenViewModel *viewModel;

@end
