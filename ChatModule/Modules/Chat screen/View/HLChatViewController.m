//
//  HLChatViewController.m
//  ChatModule
//
//  Created by Huy Le on 3/25/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import "HLChatViewController.h"
#import "HLChatScreenViewModel.h"
#import <ReactiveCocoa.h>

@interface HLChatViewController ()

@end

@implementation HLChatViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.viewModel = [[HLChatScreenViewModel alloc] initWithTextSignal:self.chatTextField.rac_textSignal];
    RAC(self.contentTextView, text) = RACObserve(self.viewModel, jsonObjectPrettyFormat);
    
    @weakify(self)
    [[self.chatTextField rac_signalForControlEvents:UIControlEventEditingDidEndOnExit] subscribeNext:^(id x) {
        @strongify(self)
        [self.view endEditing:YES];
    }];
}

@end
