//
//  HLEmotionFilterTests.m
//  ChatModule
//
//  Created by Huy Le on 3/26/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "HLEmotionChatFilter.h"
#import "HLEmotionModel.h"
#import "NSArray+FunctionalMethods.h"

@interface HLEmotionFilterTests : XCTestCase

@end

@implementation HLEmotionFilterTests

- (void)setUp
{
    
}

- (void)testSimple
{
    [self testRawContent:@"Hey (smile) please (flap) and (go) (...) (ahead)" expectResultMatch:@[@"smile",@"flap",@"go",@"ahead"]];
}

- (void)testEmotionInsideParenthessis
{
    [self testRawContent:@"Hey (abc(bcd)sdg)" expectResultMatch:@[@"bcd"]];
}

- (void)testWhitespace
{
    [self testRawContent:@"Hey (do ta), can you go (mid)?" expectResultMatch:@[@"mid"]];
}

- (void)testUppercase
{
    [self testRawContent:@"Use upper (UPPER)" expectResultMatch:@[]];
}

- (void)testBlankContent
{
    [self testRawContent:@"      " expectResultMatch:@[]];
}

- (void)testNestedParenthesis
{
    [self testRawContent:@"()()()(((((wow)))))()()()" expectResultMatch:@[@"wow"]];
}

- (void)testMixedLetter
{
    [self testRawContent:@"Use mixed (smiLEfun) lowercase (mix3NUM) & (mix3num) and uppercase (y)" expectResultMatch:@[@"y"]];
}

- (void)testSpecialCharacter
{
    [self testRawContent:@"(#) (!) (@) (2) (#) (%) (^) (&) (*) (:) :(;) (\") (   ) (-) (+) (=) )( ()" expectResultMatch:@[]];
}

- (void)testNonParenthesis
{
    [self testRawContent:@"Don't have parenthesis in this content" expectResultMatch:@[]];
}

- (void)testNil
{
    [self testRawContent:nil expectResultMatch:@[]];
}

- (void)testRawContent:(NSString *)rawContent expectResultMatch:(NSArray *)sample
{
    HLEmotionChatFilter *emotionFilter = [HLEmotionChatFilter new];
    NSArray *emotionObjects = [emotionFilter filterRawContent:rawContent];
    NSArray *result = [emotionObjects arrayByTransformingObjectsUsingBlock:^id(HLEmotionModel *emotion) {
        return emotion.content;
    }];
    XCTAssert([result isEqualToArray:sample], @"Result is not match with sample!");
}

- (void)tearDown
{
    
}

@end
