//
//  HLHyperlinkFilterTests.m
//  ChatModule
//
//  Created by Huy Le on 3/27/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "HLHyperlinkChatFilter.h"
#import "HLHyperlinkModel.h"
#import "NSArray+FunctionalMethods.h"

@interface HLHyperlinkFilterTests : XCTestCase

@end

@implementation HLHyperlinkFilterTests

- (void)setUp
{
    
}

- (void)testSimple
{
    NSString *rawContent = @"This is www.google.com http://bibi.com https://google.com";
    NSArray *sample = @[@"www.google.com",@"http://bibi.com",@"https://google.com"];
    [self testRawContent:rawContent expectResultMatch:sample];
}

- (void)shouldFail
{
    [self testRawContent:@"" expectResultMatch:@[]];
    [self testRawContent:@"http://a.b-.co" expectResultMatch:@[]];
    [self testRawContent:@"http://0.0.0.0" expectResultMatch:@[]];
    [self testRawContent:@"rdar://1234" expectResultMatch:@[]];
    [self testRawContent:@"http://-error-.invalid/" expectResultMatch:@[]];
    [self testRawContent:@"http://0.0.0.0" expectResultMatch:@[]];
    [self testRawContent:@"http://3628126748" expectResultMatch:@[]];
    [self testRawContent:@"http://?" expectResultMatch:@[]];
    [self testRawContent:@"http://##" expectResultMatch:@[]];
    [self testRawContent:@"http:///a" expectResultMatch:@[]];
    [self testRawContent:@"Use http://0.0.0.0 link" expectResultMatch:@[]];
}

- (void)testSpecialCase
{
    [self testRawContent:@"htp://goo http://.www.foo.bar/ " expectResultMatch:@[@"www.foo.bar/"]];
}

- (void)testShortURL
{
    [self testRawContent:@"Hey google.com" expectResultMatch:@[@"google.com"]];
}

- (void)testMail
{
    [self testRawContent:@"This is huy's mail: lmnhuy@gmail.com, did you got it?" expectResultMatch:@[@"lmnhuy@gmail.com"]];
}

- (void)testMailTo
{
    [self testRawContent:@"mailto:huy@gmail.com" expectResultMatch:@[@"mailto:huy@gmail.com"]];
}

- (void)testFtpProtocol
{
    [self testRawContent:@"Hey guys, my ftp is ftp://game.vn" expectResultMatch:@[@"ftp://game.vn"]];
}

- (void)testDomainURL
{
    [self testRawContent:@"google http://mail.google.com" expectResultMatch:@[@"http://mail.google.com"]];
}

- (void)testRawContent:(NSString *)rawContent expectResultMatch:(NSArray *)sample
{
    NSArray *urls = [[HLHyperlinkChatFilter new] filterRawContent:rawContent];
    XCTAssert([urls isEqualToArray:sample], @"Result is not match with sample!");
}

- (void)tearDown
{
    
}

@end
