//
//  HLMentionFilterTests.m
//  ChatModule
//
//  Created by Huy Le on 3/25/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "HLMentionChatFilter.h"
#import "HLMentionModel.h"
#import "NSArray+FunctionalMethods.h"

@interface HLMentionFilterTests : XCTestCase

@property (strong,nonatomic) HLMentionChatFilter *mentionFilter;

@end

@implementation HLMentionFilterTests

- (void)setUp
{
    self.mentionFilter = [HLMentionChatFilter new];
}

- (void)testSimple
{
    [self testRawContent:@"Hey @Bob, I am @Huy, are you there?" expectResultMatch:@[@"Bob", @"Huy"]];
}

- (void)testWithSpecialCharacter
{
    [self testRawContent:@"@#bibi @! @- @123 @_" expectResultMatch:@[@"123",@"_"]];
}

- (void)testWithInfinityAtSymbol
{
    [self testRawContent:@"@@@@@@@@@@@@@@@@@" expectResultMatch:@[]];
}

- (void)testBlankString
{
    [self testRawContent:@"      " expectResultMatch:@[]];
}

- (void)testNil
{
    [self testRawContent:nil expectResultMatch:@[]];
}

- (void)testSymbol
{
    [self testRawContent:@"@() @- @! @# @% @^ @& @* @(@ @) @, @. @: @; @' @\" @[ @] @{ @} @| @\ @? @/ @~ @" expectResultMatch:@[]];
}

- (void)testAtNearAt
{
    [self testRawContent:@"@bob@pye" expectResultMatch:@[@"bob",@"pye"]];
}

- (void)testDontHaveAt
{
    [self testRawContent:@"My name's Huy" expectResultMatch:@[]];
}

- (void)testEmail
{
    [self testRawContent:@"Hey, my email is ank@kane lmnhuy@gmail.com, lmnhuY@gmail.com lmnhuy1510@gmail.com" expectResultMatch:@[]];
}

- (void)testRawContent:(NSString *)rawContent expectResultMatch:(NSArray *)sample
{
    NSArray *mentionObjects = [self.mentionFilter filterRawContent:rawContent];
    NSArray *result = [mentionObjects arrayByTransformingObjectsUsingBlock:^id(HLMentionModel *mention) {
        return mention.content;
    }];
    XCTAssert([result isEqualToArray:sample], @"Result is not match with sample!");
}

- (void)tearDown
{
    
}

@end
