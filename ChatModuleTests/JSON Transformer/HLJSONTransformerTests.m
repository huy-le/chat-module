//
//  HLJSONTransformerTests.m
//  ChatModule
//
//  Created by Huy Le on 3/28/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "HLJSONTransformer.h"
#import "HLChatContentObject.h"
#import "HLMentionModel.h"
#import "HLHyperlinkModel.h"
#import "HLEmotionModel.h"

@interface HLJSONTransformerTests : XCTestCase

@end

@implementation HLJSONTransformerTests

- (void)testSimple
{
    HLMentionModel *mention1 = [[HLMentionModel alloc] initWithMention:@"Bibi"];
    HLMentionModel *mention2 = [[HLMentionModel alloc] initWithMention:@"Kw"];
    HLMentionModel *mention3 = [[HLMentionModel alloc] initWithMention:@"Hoang"];
    HLHyperlinkModel *link1 = [[HLHyperlinkModel alloc] initWithInfos:@{@"url":@"https://www.google.com",@"title":@"Welcome to Google!"}];
    HLEmotionModel *emotion1 = [[HLEmotionModel alloc] initWithInfos:@{@"shortcut":@"smile"}];
    NSArray *sample = @[@[mention1,mention2,mention3],@[link1],@[emotion1]];
    HLJSONTransformer *transformer = [HLJSONTransformer new];
    NSData *jsonData = [transformer jsonDataFrom:sample];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *sampleJSONString = @"{\n  \"links\" : [\n    {\n      \"url\" : \"https:\\/\\/www.google.com\",\n      \"title\" : \"Welcome to Google!\"\n    }\n  ],\n  \"emotions\" : [\n    \"smile\"\n  ],\n  \"mentions\" : [\n    \"Bibi\",\n    \"Kw\",\n    \"Hoang\"\n  ]\n}";
    XCTAssert([jsonString isEqualToString:sampleJSONString], @"JSON is not match!");
}

@end
