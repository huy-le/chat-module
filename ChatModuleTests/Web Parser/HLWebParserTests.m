//
//  HLWebParserTests.m
//  ChatModule
//
//  Created by Huy Le on 3/29/15.
//  Copyright (c) 2015 Huy Le. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "HLWebParser.h"

@interface HLWebParserTests : XCTestCase

@end

@implementation HLWebParserTests

- (void)testSimple
{
    //Expectation
    XCTestExpectation *expectation = [self expectationWithDescription:@"Testing Async Method Works!"];
    [[HLWebParser new] titleOfURL:@"https://www.google.com" success:^(NSString *title, NSString *url) {
        [expectation fulfill];
        XCTAssert([title isEqualToString:@"Google"],@"Not match Google title!");
    } failure:^(NSError *error, NSString *url) {
        
    }];
    
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if(error) {
            XCTFail(@"Expectation Failed with error: %@", error);
        }
    }];
}

@end
