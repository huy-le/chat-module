# Chat Module #

Write code that **takes a chat message string (INPUT)** and **returns a JSON string (OUTPUT)** containing information about its contents.

Special content to look for includes:

- `@mentions` - A way to mention a user. Always **starts** with an '@' and **ends when hitting a non-word character.** (http://help.hipchat.com/knowledgebase/articles/64429-how-do-mentions-work-)
( A word character is a character from a-z, A-Z, 0-9, including the _ (underscore) character.)

- Emoticons - For this exercise, you ONLY need to consider **custom emoticons** which are **ASCII strings**, **no longer than 15 characters**, **contained in parenthesis**. You can assume that anything matching this format is an emoticon. (https://www.hipchat.com/emoticons)

- Links - **Any URLs** contained in the message, along with the **page's title**.

### Summary ###

#### Behavior

Whenever user **pause** typing on TextField in **1s**, the app will scan TextField content, then update the JSON TextView. The UI will be updated as soon as possible whenever the engine found the special content which be defined.

The Link contents only appear when it found the title of that URL.

#### Architecture

![Screen Shot 2015-03-30 at 10.17.08.png](https://bitbucket.org/repo/b9xGAj/images/3269494327-Screen%20Shot%202015-03-30%20at%2010.17.08.png)

#### ReactiveCocoa flow

![Screen Shot 2015-03-30 at 10.29.15.png](https://bitbucket.org/repo/b9xGAj/images/2519101299-Screen%20Shot%202015-03-30%20at%2010.29.15.png)